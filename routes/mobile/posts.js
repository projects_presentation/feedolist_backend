var moment = require('moment');
var express = require('express');
var router = express.Router();
var instagramParser = require('../../utilities/instagramParser');

var usersDb = require('../../mysql/users');
var postsDb = require('../../mysql/posts');

router.post('/', (req, res) => {
    //res.status(200).send(null);
    //return;

    if (req.query.username) {
        instagramParser.getUserData(req.query.username)
            .then(result => {
                if (result.posts.length != 0) {
                    result.posts.forEach(element => {
                        element.userId = result.user.id;
                        element.username = req.query.username;
                        element.date = moment.unix(element.date).format("YYYY-MM-DD HH:mm:ss");
                        postsDb.insert(element)
                            .then(() => {
                                //console.log('Client data was successfully inserted!');
                            })
                            .catch(error => {
                                console.log(error);
                            });
                    });
                    res.status(200).send(null);
                } else {
                    console.log('Error getting user data! Data length is zero!');
                    res.status(500).send(null);
                }
            })
            .catch(error => {
                console.log(error);
                res.status(500).send(null);
            });
    }

});

router.get('/', (req, res) => {
    if (req.query.username) {
        var username = req.query.username;
        usersDb.getone(username)
            .then(user => {
                postsDb.get(username)
                    .then(posts => {
                        res.status(200).send({
                            user: user,
                            posts: posts
                        });
                    }).catch(error => {
                        console.log(error);
                        res.status(500).send(null);
                    });
            }).catch(error => {
                console.log(error);
                res.status(500).send(null);
            });
    } else {
        console.log("Error getting user posts, username [" + username + "] is invalid!");
        res.status(500).send(null);
    }
});

module.exports = router;