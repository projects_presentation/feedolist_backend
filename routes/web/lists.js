var express = require('express');
var router = express.Router();
var async = require('async');

var listsDb = require('../../mysql/lists');

router.get('/top_lists', function(req, res) {

    listsDb.getAllTopLists()
        .then(result => {
            res.status(200).send({
                top_lists: result
            });
        })
        .catch(error => {
            console.log(error);
            res.status(503).send(null);
        });
});

router.get('/top_last_lists', function(req, res) {
    listsDb.getTopLastLists()
        .then(result => {
            res.status(200).send({
                top_last_lists: result
            });
        })
        .catch(error => {
            console.log(error);
            res.status(503).send(null);
        });
});

module.exports = router;