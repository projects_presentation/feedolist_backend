var moment = require('moment');
var express = require('express');
var router = express.Router();
var instagramParser = require('../../utilities/instagramParser');

var listsDb = require('../../mysql/lists');
var usersDb = require('../../mysql/users');
var postsDb = require('../../mysql/posts');

function updateUserPosts(username) {
    instagramParser.getUserData(username)
        .then(result => {
            if (result.posts.length != 0) {
                result.posts.forEach(posts => {
                    posts.userId = result.user.id;
                    posts.username = username;
                    posts.date = moment.unix(posts.date).format("YYYY-MM-DD HH:mm:ss");
                    postsDb.insert(posts)
                        .then(() => {
                            //console.log('Client data was successfully inserted!');
                        })
                        .catch(error => {
                            console.log(error);
                        });
                });
            } else {
                console.log('Error getting user data! Data length is zero!');
            }
        })
        .catch(error => {
            console.log(error);
        });
}

router.get('/', (req, res) => {
    if (req.query.user) {
        var usernamesList = req.query.user;

        var users = [];
        if (Array.isArray(usernamesList)) {
            users = usernamesList;
        } else {
            users.push(usernamesList);
        }

        var queueLength = users.length;

        var usersInPosts = [];
        var postsList = [];

        listsDb.updateTopLists(req.url, users);

        users.forEach(usernameInList => {
            updateUserPosts(usernameInList);

            usersDb.getone(usernameInList)
                .then(user => {
                    usersInPosts.push(user);
                    postsDb.get(usernameInList)
                        .then(posts => {
                            postsList.push({
                                posts: posts
                            });
                            if (--queueLength == 0) {
                                res.status(200).send({
                                    users: usersInPosts,
                                    postsList: postsList
                                });
                            }
                        }).catch(error => {
                            console.log(error);
                        });
                }).catch(error => {
                    console.log(error);
                    res.status(500).send(null);
                });
        });
    } else {
        console.log("Error getting user posts, username [" + usernamesList + "] is invalid!");
        res.status(500).send(null);
    }
});

module.exports = router;