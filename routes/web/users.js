var moment = require('moment');
var express = require('express');
var router = express.Router();
var instagramParser = require('../../utilities/instagramParser');

var usersDb = require('../../mysql/users');

router.get('/users_list', (req, res) => {
    if (req.query.username) {
        var username = req.query.username;
        usersDb.getall(username)
            .then(users => {
                res.status(200).jsonp({
                    users: users
                });
                /*
                res.status(200).send({
                    users: users
                });
                */
            }).catch(error => {
                console.log('Error searching for users on database: ' + error);
                res.status(500).send(null);
            });
    } else {
        console.log("Error getting user data, username [" + username + "] is invalid!");
        res.status(500).send(null);
    }
});

module.exports = router;