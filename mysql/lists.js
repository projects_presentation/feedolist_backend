var mysql = require('./mysql');

function updateTopLists(url, listOfUsers) {

    var users = '';
    listOfUsers.forEach(user => {
        users += '@' + user + ' ';
    });
    var query = "INSERT INTO top_lists (url,users) VALUES ('" + url + "','" + users + "') ON DUPLICATE KEY UPDATE count = count + 1;";
    var pool = mysql.getPool();
    pool.getConnection(function(err, connection) {
        if (err) {
            console.log('Error in MySQL while updating top lists: ' + err);
        } else {
            connection.query(query, function(err, result) {
                connection.release();
                if (err) {
                    console.log('Error in MySQL while updating top lists: ' + err);
                }
            });
        }
    });
}

function getAllTopLists() {

    return new Promise((resolve, reject) => {
        var query = "SELECT * FROM top_lists ORDER BY count DESC LIMIT 100";
        var pool = mysql.getPool();
        pool.getConnection(function(err, connection) {
            if (err) {
                reject('Error getting top lists: ' + err);
            } else {
                connection.query(query, function(err, rows) {
                    connection.release();
                    if (err) {
                        reject('Error getting top lists: ' + err);
                    } else {
                        resolve(rows);
                    }
                });
            }
        });
    })
};

function getTopLastLists() {

    return new Promise((resolve, reject) => {
        var query = "SELECT * FROM top_lists ORDER BY updated DESC LIMIT 100";
        var pool = mysql.getPool();
        pool.getConnection(function(err, connection) {
            if (err) {
                reject('Error getting top last lists: ' + err);
            } else {
                connection.query(query, function(err, rows) {
                    connection.release();
                    if (err) {
                        reject('Error getting top last lists: ' + err);
                    } else {
                        resolve(rows);
                    }
                });
            }
        });
    })
};

module.exports.updateTopLists = updateTopLists;
module.exports.getAllTopLists = getAllTopLists;
module.exports.getTopLastLists = getTopLastLists;