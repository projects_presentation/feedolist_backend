var mysql = require('./mysql');

function get(username) {

    return new Promise((resolve, reject) => {
        var query = "SELECT * FROM posts WHERE username='" + username + "' ORDER BY date DESC LIMIT 50";
        var pool = mysql.getPool();
        pool.getConnection(function(err, connection) {
            if (err) {
                reject('Error getting user: ' + username + ' data from posts! ' + err);
            } else {
                connection.query(query, function(err, rows) {
                    connection.release();
                    if (err) {
                        reject('Error getting user: ' + username + ' data from posts! ' + err);
                    } else {
                        resolve(rows);
                    }
                });
            }
        });
    });
}

function insert(post) {

    return new Promise((resolve, reject) => {
        var query = "INSERT INTO posts SET ? ON DUPLICATE KEY UPDATE likes = '" + post.likes + "', imageUrl = '" + post.imageUrl + "'";
        var pool = mysql.getPool();
        pool.getConnection(function(err, connection) {
            if (err) {
                reject('Error inserting new post: ' + err);
            } else {
                connection.query(query, post, function(err, result) {
                    connection.release();
                    if (err) {
                        reject('Error inserting new post: ' + err);
                    } else {
                        resolve(result.affectedRows != 0);
                    }
                });
            }
        });
    });
}

module.exports.get = get;
module.exports.insert = insert;