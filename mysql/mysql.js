var mysql = require('mysql');
var mysqlSettings;

var poolCreated = false;
var pool;

var mysqlSettings = {
    host: 'localhost',
    user: 'feedolist',
    password: 'F33d0l1stMySQL',
    database: 'feedolist',
    charset: 'utf8mb4'
};

function connection() {
    return mysql.createConnection(mysqlSettings);
}

function getPool() {
    if (poolCreated) {
        return pool;
    } else {
        poolCreated = true;
        pool = mysql.createPool(mysqlSettings);
        return pool;
    }
}

module.exports.mysqlSettings = mysqlSettings;
module.exports.connection = connection;
module.exports.getPool = getPool;