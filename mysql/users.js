var mysql = require('./mysql');

var instagramParser = require('../utilities/instagramParser');

function getone(username) {
    return new Promise((resolve, reject) => {
        var query = "SELECT * FROM users WHERE username='" + username + "' LIMIT 1";
        var pool = mysql.getPool();
        pool.getConnection(function(err, connection) {
            if (err) {
                reject('Error getting user: ' + username + ' data! ' + err);
            } else {
                connection.query(query, function(err, rows) {
                    connection.release();
                    if (err) {
                        reject('Error getting user: ' + username + ' data! ' + err);
                    } else {
                        resolve(rows[0]);
                    }
                });
            }
        });
    });
}

function getall(filter) {
    return new Promise((resolve, reject) => {
        var query = "SELECT * FROM users WHERE username LIKE '" + filter + "%' OR name LIKE '" + filter + "%' ORDER BY name ASC LIMIT 10";
        var pool = mysql.getPool();
        pool.getConnection(function(err, connection) {
            if (err) {
                reject('Error getting user data by filter : ' + filter + ' ' + err);
            } else {
                connection.query(query, function(err, rows) {
                    connection.release();
                    if (err) {
                        reject('Error getting user data by filter : ' + filter + ' ' + err);
                    } else {
                        console.log(rows);
                        resolve(rows);
                    }
                });
            }
        });
    });
}

function insert(user) {

    return new Promise((resolve, reject) => {

        var query = "INSERT INTO users SET ? ON DUPLICATE KEY UPDATE name = '" + user.name + "', picture = '" + user.picture + "'";
        var pool = mysql.getPool();
        pool.getConnection(function(err, connection) {
            if (err) {
                console.log('Error inserting or updating new user: ' + err);
                reject('Error inserting or updating new user: ' + err);
            } else {
                connection.query(query, user, function(err, result) {
                    connection.release();
                    if (err) {
                        console.log('Error inserting or updating new user: ' + err);
                        reject('Error inserting or updating new user: ' + err);
                    } else {
                        console.log('User: ' + user.username + ' was successfully updated!');
                        resolve(result.affectedRows != 0);
                    }
                });
            }
        });
    })

}

module.exports.getone = getone;
module.exports.getall = getall;
module.exports.insert = insert;