var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');

var mobileUsers = require('./routes/mobile/users');
var mobilePosts = require('./routes/mobile/posts');

var webUsers = require('./routes/web/users');
var webPosts = require('./routes/web/posts');
var webLists = require('./routes/web/lists');

/* HTTPS */
var https = require('https');
var http = require('http');
var fs = require('fs');

/* CORS */
var cors = require('cors')

// This line is from the Node.js HTTPS documentation.
var options = {
    key: fs.readFileSync('./certificates/privkey.pem', 'utf8'),
    cert: fs.readFileSync('./certificates/cert.pem', 'utf8'),
    ca: fs.readFileSync('./certificates/fullchain.pem', 'utf8')
};



var app = express();

app.use(cors());

app.enable('trust proxy');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Add headers
app.use(function(req, res, next) {

    // Website you wish to allow to connect

    res.setHeader('Access-Control-Allow-Origin', 'https://www.feedolist.com');
    //res.setHeader('Access-Control-Allow-Origin', 'http://5.189.163.118:4000');
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4000');

    // Request methods you wish to allow
    //res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    //res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    //res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use('/mobile/users', mobileUsers);
app.use('/mobile/posts', mobilePosts);

app.use('/', webUsers);
app.use('/web/posts', webPosts);
app.use('/web/lists', webLists);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found 404');
    err.status = 404;
    next(err);
});

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    console.log(err);
    res.status(404).send(null);
});


var port = 443;

https.createServer(options, app).listen(port);
console.log('Server is running at port: ' + port);
/*
app.listen(process.env.port || port, function(result) {
    console.log('Server is running at port: ' + port);
});
*/
module.exports = app;