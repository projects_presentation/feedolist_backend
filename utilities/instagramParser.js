var request = require('request');
var User = require('../models/user').User;
var Post = require('../models/post').Post;

var usersDb = require('../mysql/users');

var address = process.env.NODE_ENV === 'development' ? 'http://localhost:3000' : 'https://www.feedolist.com';

var startString = '<script type="text/javascript">window._sharedData = ';
var endString = ';</script>';

function getUserData(username) {
    return new Promise((resolve, reject) => {
        request.get(
            uri = 'https://www.instagram.com/' + username + '/',
            function(error, response, body) {

                try {
                    var startIndex = body.indexOf(startString) + startString.length;
                    var endIndex = body.indexOf(endString);
                    console.log("Received body: " + body.substring(startIndex, endIndex));
                    var parsedJSON = JSON.parse(body.substring(startIndex, endIndex));
                    parseUserPersonalData(parsedJSON)
                        .then(user => {
                            parseUserposts(parsedJSON)
                                .then(posts => {
                                    resolve({
                                        user: user,
                                        posts: posts
                                    });
                                })
                                .catch(error => {
                                    reject(error);
                                });
                        })
                        .catch(error => {
                            reject(error);
                        });
                } catch (error) {
                    reject(error);
                }
            }
        );
    });
}



function parseUserPersonalData(parsedJSON) {
    return new Promise((resolve, reject) => {
        if (parsedJSON.entry_data.ProfilePage && parsedJSON.entry_data.ProfilePage !== undefined) {
            try {
                var id = parsedJSON.entry_data.ProfilePage[0].graphql.user.id;
                var username = parsedJSON.entry_data.ProfilePage[0].graphql.user.username;
                var name = parsedJSON.entry_data.ProfilePage[0].graphql.user.full_name == null ? "" : parsedJSON.entry_data.ProfilePage[0].graphql.user.full_name;
                var picture = parsedJSON.entry_data.ProfilePage[0].graphql.user.profile_pic_url_hd;

                var user = new User(id, username, name, picture);
                usersDb.insert(user);
                resolve(user);
            } catch (error) {
                reject('Error parsing user personal data: ' + error);
            }
        } else {
            reject('Error parsing user personal data! Instagram returned no data!');
        }
    });
}

function parseUserposts(parsedJSON) {

    return new Promise((resolve, reject) => {
        try {
            var postsList = [];
            var posts = parsedJSON.entry_data.ProfilePage[0].graphql.user.edge_owner_to_timeline_media.edges;
            for (var i = 0; i < posts.length; ++i) {
                if (posts[i].node.is_video) continue;
                var text = "";
                if (posts[i].node.edge_media_to_caption.edges[0] !== undefined) {
                    if (posts[i].node.edge_media_to_caption.edges[0].node.text === undefined) {
                        text = "";
                    } else {
                        text = posts[i].node.edge_media_to_caption.edges[0].node.text;
                    }
                }
                var id = posts[i].node.id;
                var likes = posts[i].node.edge_liked_by.count;
                var date = posts[i].node.taken_at_timestamp;
                var imageUrl = posts[i].node.display_url;
                var dimensions = posts[i].node.dimensions;
                var post = new Post(id, text, likes, date, imageUrl, dimensions.height, dimensions.width);
                postsList.push(post);
                resolve(postsList);
            }
        } catch (error) {
            reject('Error getting user: ' + username + ' post: ' + error.message);
        }
    });
}

module.exports.getUserData = getUserData;