function Post(id, text, likes, date, imageUrl, height, width) {
    this.id = id;
    this.text = text;
    this.likes = likes;
    this.date = date;
    this.imageUrl = imageUrl;
    this.height = height;
    this.width = width;
}

module.exports.Post = Post;