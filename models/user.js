function User(id, username, name, picture) {
    this.id = id;
    this.username = username;
    this.name = name;
    this.picture = picture;
}

module.exports.User = User;